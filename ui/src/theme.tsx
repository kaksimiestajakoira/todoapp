export default {
  light: 'whitesmoke',
  dark: '#0747A6',
  darkFade: '#0962e6'
};
