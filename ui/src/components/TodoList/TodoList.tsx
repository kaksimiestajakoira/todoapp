import React from 'react';
import styled from 'styled-components';

interface TodoListProps { 
  items: Array<string>;
  header: string;
  addTodo: (todo: string) => void;
  addingTodo: boolean;
}
interface TodoListState { }

class TodoList extends React.Component<TodoListProps, TodoListState> {
  render() {
    const ItemListContainer = styled.div`
      padding: 0 1em;
      box-sizing: border-box;
    `;

    const ItemList = styled.ul`
      list-style: square;
    `;

    const LoadingText = styled.span`
      font-size: .7em;
    `;

    var items = this.props.items.map(function (item: string, index: number) {
      return (
        <li key={index}>{item}</li>
      );
    });

    const addingTodo = this.props.addingTodo;

    return (
      <ItemListContainer>
        <h3>{this.props.header}</h3>
        <ItemList>
          {items}
        </ItemList>
        {addingTodo ? (
          <LoadingText>Adding todo...</LoadingText>
        ) : (
          <button 
            onClick={() => {
              this.props.addTodo('test');
            }}
          >
            Add todo
          </button>
        )}
      </ItemListContainer>
    );
  }
}

export default TodoList;
