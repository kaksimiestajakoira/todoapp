import React from 'react';
import styled from 'styled-components';
import theme from 'theme';

class RootLayout extends React.Component {
  render() {
    const Main = styled.main`
      height: 100%;
      color: ${theme.dark};
    `;

    return [
      (
        <Main key="main" role="main">
          {this.props.children}
        </Main>
      )
    ];
  }
}

export default RootLayout;
