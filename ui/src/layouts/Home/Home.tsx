import React from 'react';
import { bindActionCreators, ActionCreator, Dispatch, Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { connect } from 'react-redux';
import { RootState } from 'types';
import { fetchMyTodos, fetchSharedTodos, addMyTodo, addSharedTodo } from 'actions/todos';
import styled from 'styled-components';
import theme from 'theme';
import RootLayout from 'layouts/RootLayout';
import TodoList from 'components/TodoList';
import Spinner from 'components/Spinner';

namespace HomeView {
  export interface Props {
    fetchMyTodos: ActionCreator<ThunkAction<Promise<Action>, RootState, void>>;
    fetchSharedTodos: ActionCreator<ThunkAction<Promise<Action>, RootState, void>>;
    addMyTodo: ActionCreator<ThunkAction<Promise<Action>, RootState, void>>;
    addSharedTodo: ActionCreator<ThunkAction<Promise<Action>, RootState, void>>;
    loadingMyTodos: boolean;
    loadingSharedTodos: boolean;
    addingMyTodo: boolean;
    addingSharedTodo: boolean;
    myTodos: Array<string>;
    sharedTodos: Array<string>;
    error: boolean;  
  }
  
  export interface State {}
}

class HomeView extends React.Component<HomeView.Props, HomeView.State> {
  componentDidMount() {
    this.props.fetchMyTodos();
    this.props.fetchSharedTodos();
  }

  render() {
    const ColumnContainer = styled.div`
      display: flex;
      height: 100%;

      .column + .column {
        border-left: 1px solid #ccc;
      }
    `;

    const Column = styled.div`
      flex: 1 0 0;
    `;

    const SideColumn = styled(Column)`
      background: linear-gradient(to bottom, ${theme.dark}, ${theme.darkFade});
      text-align: center;
      flex: .5 0 0;
      padding: 0 1em;
    `;

    const Title = styled.h1`
      color: ${theme.light};
    `;

    const loadingMyTodos = this.props.loadingMyTodos;
    const loadingSharedTodos = this.props.loadingSharedTodos;
    const myTodos = this.props.myTodos;
    const sharedTodos = this.props.sharedTodos;

    return (
      <RootLayout>
          <ColumnContainer>
            <SideColumn>
              <Title>Todo Application</Title>
            </SideColumn>
            <Column className="column">
            {loadingMyTodos ? (
                <Spinner />
              ) : (
              <TodoList 
                header="My Todos"
                items={myTodos}
                addTodo={this.props.addMyTodo}
                addingTodo={this.props.addingMyTodo}
              />
            )}
            </Column>
            <Column className="column">
            {loadingSharedTodos ? (
                <Spinner />
              ) : (
              <TodoList 
                header="Shared Todos"
                items={sharedTodos}
                addTodo={this.props.addSharedTodo}
                addingTodo={this.props.addingSharedTodo}
              />
            )}
            </Column>
          </ColumnContainer>
      </RootLayout>
    );
  }
}

const mapStateToProps = (state: RootState): Partial<HomeView.Props> => ({
  loadingMyTodos: state.todos.loadingMyTodos,
  loadingSharedTodos: state.todos.loadingSharedTodos,
  addingMyTodo: state.todos.addingMyTodo,
  addingSharedTodo: state.todos.addingSharedTodo,
  error: state.todos.error,
  myTodos: state.todos.myTodos,
  sharedTodos: state.todos.sharedTodos
});

// Turns an object whose values are action creators, into an object with the same keys,
// but with every action creator wrapped into a dispatch call so they may be invoked directly.
const mapDispatchToProps = (dispatch: Dispatch<RootState>) => bindActionCreators(
  {
    fetchMyTodos,
    fetchSharedTodos,
    addMyTodo,
    addSharedTodo
  },
  dispatch
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeView);
