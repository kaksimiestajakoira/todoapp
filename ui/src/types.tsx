// state types
export interface RootState {
  todos: TodosState;
}

export interface TodosState {
  loadingMyTodos: boolean;
  loadingSharedTodos: boolean;
  addingMyTodo: boolean;
  addingSharedTodo: boolean;
  myTodos: Array<string>;
  sharedTodos: Array<string>;
  error: boolean;
}

export interface PayloadAction<T> {
  type: string;
  payload: T;
}

export type GetTodosPayload = Array<string>;
