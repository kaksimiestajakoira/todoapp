import * as React from 'react';
import { Provider } from 'react-redux';
import store from './store';
import Home from 'layouts/Home/';
import './App.css';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Home />
      </Provider>
    );
  }
}

export default App;
