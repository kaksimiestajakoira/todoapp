import axios, { AxiosResponse } from 'axios';

const todoApi = {
  addTodo: (todo: string): Promise<void> => {

    return axios.get('/api/v1/ping')
      .then((response: AxiosResponse) => {
        console.log(response);
        return response.data;
      })
      .catch((error: string) => {
        console.log(error);
      });
  } 
};

export default todoApi;
