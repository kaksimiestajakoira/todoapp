import { Action, ActionCreator, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { PayloadAction, RootState, GetTodosPayload } from 'types';
import api from 'api/todo';

export const LOADING_MY_TODOS = 'LOADING_MY_TODOS';
export type LOADING_MY_TODOS_TYPE = typeof LOADING_MY_TODOS;
interface LoadingMyTodosAction extends Action {
  readonly type: LOADING_MY_TODOS_TYPE;
}

export const GET_MY_TODOS = 'GET_MY_TODOS';
export type GET_MY_TODOS_TYPE = typeof GET_MY_TODOS;
interface GetMyTodosAction extends PayloadAction<GetTodosPayload> {
  readonly type: GET_MY_TODOS_TYPE;
  payload: GetTodosPayload;
}

export const LOADING_SHARED_TODOS = 'LOADING_SHARED_TODOS';
export type LOADING_SHARED_TODOS_TYPE = typeof LOADING_SHARED_TODOS;
interface LoadingSharedTodosAction extends Action {
  readonly type: LOADING_SHARED_TODOS_TYPE;
}

export const GET_SHARED_TODOS = 'GET_SHARED_TODOS';
export type GET_SHARED_TODOS_TYPE = typeof GET_SHARED_TODOS;
interface GetSharedTodosAction extends PayloadAction<GetTodosPayload> {
  readonly type: GET_SHARED_TODOS_TYPE;
  payload: GetTodosPayload;
}

export const ADD_MY_TODO_LOADING = 'ADD_MY_TODO_LOADING';
export type ADD_MY_TODO_LOADING_TYPE = typeof ADD_MY_TODO_LOADING;
interface LoadingAddMyTodoAction extends Action {
  readonly type: ADD_MY_TODO_LOADING_TYPE;
}

export const ADD_MY_TODO_SUCCESS = 'ADD_MY_TODO_SUCCESS';
export type ADD_MY_TODO_SUCCESS_TYPE = typeof ADD_MY_TODO_SUCCESS;
interface AddMyTodoSuccessAction {
  readonly type: ADD_MY_TODO_SUCCESS_TYPE;
  payload: string;
}

export const ADD_MY_TODO_FAILURE = 'ADD_MY_TODO_FAILURE';
export type ADD_MY_TODO_FAILURE_TYPE = typeof ADD_MY_TODO_FAILURE;
interface AddMyTodoFailureAction {
  readonly type: ADD_MY_TODO_FAILURE_TYPE;
  payload: string;
}

export const ADD_SHARED_TODO_LOADING = 'ADD_SHARED_TODO_LOADING';
export type ADD_SHARED_TODO_LOADING_TYPE = typeof ADD_SHARED_TODO_LOADING;
interface LoadingAddSharedTodoAction extends Action {
  readonly type: ADD_SHARED_TODO_LOADING_TYPE;
}

export const ADD_SHARED_TODO_SUCCESS = 'ADD_SHARED_TODO_SUCCESS';
export type ADD_SHARED_TODO_SUCCESS_TYPE = typeof ADD_SHARED_TODO_SUCCESS;
interface AddSharedTodoSuccessAction {
  readonly type: ADD_SHARED_TODO_SUCCESS_TYPE;
  payload: string;
}

export const ADD_SHARED_TODO_FAILURE = 'ADD_SHARED_TODO_FAILURE';
export type ADD_SHARED_TODO_FAILURE_TYPE = typeof ADD_SHARED_TODO_FAILURE;
interface AddSharedTodoFailureAction {
  readonly type: ADD_SHARED_TODO_FAILURE_TYPE;
  payload: string;
}

export type TodoAction = LoadingMyTodosAction 
                        | GetMyTodosAction 
                        | LoadingSharedTodosAction 
                        | GetSharedTodosAction 
                        | LoadingAddMyTodoAction
                        | AddMyTodoSuccessAction
                        | AddMyTodoFailureAction
                        | LoadingAddSharedTodoAction
                        | AddSharedTodoSuccessAction
                        | AddSharedTodoFailureAction;

export const fetchMyTodos: ActionCreator<
  ThunkAction<Promise<Action>, RootState, void>
  > = () => {
    return async (dispatch: Dispatch<RootState>): Promise<Action> => {
      dispatch({
        type: LOADING_MY_TODOS
      });

      const todos = await resolveAfterSeconds(
        [
          'My todo1',
          'My todo2',
          'My todo3'
        ],
        1500
      );
      return dispatch({
        type: GET_MY_TODOS,
        payload: todos
      });
    };
  };

export const fetchSharedTodos: ActionCreator<
  ThunkAction<Promise<Action>, RootState, void>
  > = () => {
    return async (dispatch: Dispatch<RootState>): Promise<Action> => {
      dispatch({
        type: LOADING_SHARED_TODOS
      });

      const todos = await resolveAfterSeconds(
        [
          'Shared todo1',
          'Shared todo2',
          'Shared todo3'
        ],
        2000
      );
      return dispatch({
        type: GET_SHARED_TODOS,
        payload: todos
      });
    };
  };

// temporary helper function to mock backend request
function resolveAfterSeconds(x: GetTodosPayload, milliseconds: number) {
  return new Promise(resolve => {
    setTimeout(
      () => {
        resolve(x);
      },
      milliseconds);
  });
}

export const addMyTodo: ActionCreator<
  ThunkAction<Promise<Action>, RootState, void>
  > = (todo) => {
    return async (dispatch: Dispatch<RootState>): Promise<Action> => {
      dispatch({
        type: ADD_MY_TODO_LOADING
      });

      return api.addTodo(todo)
        .then((response) => {
          console.log('TODO ADDED', todo);
          return dispatch({
            type: ADD_MY_TODO_SUCCESS,
            payload: response
          });
        })
        .catch(response => {
          console.log('TODO FAILED', todo);
          return dispatch({
            type: ADD_MY_TODO_FAILURE
          });
        });
    };
  };

export const addSharedTodo: ActionCreator<
  ThunkAction<Promise<Action>, RootState, void>
  > = (todo) => {
    return async (dispatch: Dispatch<RootState>): Promise<Action> => {
      dispatch({
        type: ADD_SHARED_TODO_LOADING
      });

      return api.addTodo(todo)
        .then((response) => {
          return dispatch({
            type: ADD_SHARED_TODO_SUCCESS,
            payload: response
          });
        })
        .catch(response => {
          return dispatch({
            type: ADD_SHARED_TODO_FAILURE
          });
        });
    };
  };
