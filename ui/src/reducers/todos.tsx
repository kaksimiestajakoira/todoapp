import { 
  LOADING_MY_TODOS, 
  GET_MY_TODOS, 
  LOADING_SHARED_TODOS, 
  GET_SHARED_TODOS,
  ADD_MY_TODO_LOADING,
  ADD_MY_TODO_SUCCESS,
  ADD_MY_TODO_FAILURE,
  ADD_SHARED_TODO_LOADING,
  ADD_SHARED_TODO_SUCCESS,
  ADD_SHARED_TODO_FAILURE,
  TodoAction } from 'actions/todos';
import { TodosState } from 'types';

const initialState = {
  loadingMyTodos: false,
  loadingSharedTodos: false,
  addingMyTodo: false,
  addingSharedTodo: false,
  error: false,
  myTodos: [],
  sharedTodos: []
};

export default (state: TodosState = initialState, action: TodoAction) => {
  switch (action.type) {
    case LOADING_MY_TODOS:
      return {
        ...state,
        loadingMyTodos: true
      };
    case LOADING_SHARED_TODOS:
      return {
        ...state,
        loadingSharedTodos: true
      };
    case GET_MY_TODOS:
      return {
        ...state,
        myTodos: action.payload,
        loadingMyTodos: false
      };
    case GET_SHARED_TODOS:
      return {
        ...state,
        sharedTodos: action.payload,
        loadingSharedTodos: false
      };
    case ADD_MY_TODO_LOADING:
      return {
        ...state,
        addingMyTodo: true
      };
    case ADD_MY_TODO_SUCCESS:
      let myTodos = state.myTodos.slice(0);
      myTodos.push(action.payload);
      return {
        ...state,
        myTodos: myTodos,
        addingMyTodo: false
      };
    case ADD_MY_TODO_FAILURE:
      return {
        ...state,
        newTodo: action.payload,
        addingMyTodo: false
      };
    case ADD_SHARED_TODO_LOADING:
      return {
        ...state,
        addingSharedTodo: true
      };
    case ADD_SHARED_TODO_SUCCESS:
      let sharedTodos = state.sharedTodos.slice(0);
      sharedTodos.push(action.payload);
      return {
        ...state,
        sharedTodos: sharedTodos,
        addingSharedTodo: false
      };
    case ADD_SHARED_TODO_FAILURE:
      return {
        ...state,
        newTodo: action.payload,
        addingSharedTodo: false
      };
    default:
      return state;
  }
};
