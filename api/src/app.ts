import { ServerLoader, ServerSettings, GlobalAcceptMimesMiddleware } from "@tsed/common";
import * as Path from "path";
import * as express from "express";
import { $log } from "ts-log-debug";
import { MongooseService } from "./services/mongoose.service";
import * as cookieParser from "cookie-parser";
import * as bodyParser from "body-parser";
import * as compression from "compression";
import * as methodOverride from "method-override";
import "@tsed/swagger";

const rootDir = Path.resolve(__dirname);
@ServerSettings({
  rootDir,
  mount: {
    "/api/v1": `${rootDir}/controllers/**/**.controller.v1.ts`,
    "/api/v2": `${rootDir}/controllers/**/**.controller.v2.ts`
  },
  componentsScan: [
    `${rootDir}/services/**/**.service.ts`,
    `${rootDir}/middlewares/**/**.ts`
  ],
  httpPort: process.env.PORT || 3000,
  acceptMimes: ["application/json"],
  swagger: {
    path: "/api-docs"

  }
})
export class Server extends ServerLoader {

  private testMode: boolean;

  constructor(test: boolean = false) {
    super();
    this.testMode = test;
  }
  /**
   * This method let you configure the middleware required by your application to works.
   * @returns {Server}
   */
  public async $onMountingMiddlewares(): Promise<any> {
    this
      .use(GlobalAcceptMimesMiddleware)
      .use(cookieParser())
      .use(compression({}))
      .use(methodOverride())
      .use(bodyParser.json())
      .use(bodyParser.urlencoded({
          extended: true,
      }));

    return null;
  }

  async $onInit(): Promise<any> {
    this.testMode || await MongooseService.connect();
    $log.debug("DB connected");
  }

  public $onReady() {
    $log.info("Server started...");
  }

  public $onServerInitError(error: Error) {
    $log.error(error.message);
  }
}
