import { expect } from "chai";
import { inject, bootstrap, Done } from "@tsed/testing";
import { ControllerService, ExpressApplication } from "@tsed/common";
import { PingController } from "controllers/ping.controller.v1";
import { Server } from "app";
import * as SuperTest from "supertest";
import { $log } from "ts-log-debug";

describe('PingController', () => {
  // bootstrap your Server to load all endpoints before run your test
  beforeEach(bootstrap(Server, true));

  it('should exist', inject([ControllerService], (controllerService: ControllerService) => {
    const instance: PingController = controllerService.invoke<PingController>(PingController);
    expect(!!instance).to.be.true;
  }));
});

describe("Rest", () => {
  // bootstrap your Server to load all endpoints before run your test
  beforeEach(bootstrap(Server));

  describe("GET /api/v1/ping", () => {
    before(() => {
      $log.level = "OFF"
  });
    it("should return pong!", inject([ExpressApplication, Done], (expressApplication: ExpressApplication, done: Done) => {
      SuperTest(expressApplication)
          .get("/api/v1/ping")
          .expect(200)
          .end((err, response: any) => {
              if (err) {
                  throw (err);
              }

              expect(`${response.text}`).to.be.eq("pong!");
              done();
          });
    }));
  });
});
