import { Controller, Get, PathParams, BodyParams, Post, Put, Delete } from "@tsed/common";
import { Description, Summary, Returns } from "@tsed/swagger";
import { TodoItem } from "interfaces/todo";
import { MongooseService } from "services/mongoose.service";
import { InsertOneWriteOpResult } from "mongodb";
import { isNil } from "lodash";
import { $log } from "ts-log-debug";

@Controller("/todo")
export class TodoController {

  /**
   * Get all todos
   *
   * @returns {Promise<string>}
   * @memberof TodoController
   */
  @Get("")
  @Description("Get all todo items")
  @Summary("Get all todo items")
  @Returns(200, { description: "OK" })
  async getAllTodos(): Promise<Array<TodoItem>> {
    // FIXME: Add error handling!
    const collection = MongooseService.resource.collection('todo');
    return await collection.find({}).toArray() as Array<TodoItem>;
  }

  /**
   * Get single todo by id.
   *
   * @returns {Promise<string>}
   * @memberof TodoController
   */
  @Get("/:id")
  @Description("Get todo item by ID")
  @Summary("Get todo item by ID")
  @Returns(200, { description: "OK" })
  @Returns(404, { description: "Todo item not found" })
  async getTodo(@PathParams("id") id: string): Promise<string> {
    // TODO: Implementation.
    return null;
  }

  /**
   * Add new todo item.
   *
   * @param {Todo} todo
   * @returns {Promise<Todo>}
   * @memberof TodoController
   */
  @Post("")
  @Description("Create new todo item")
  @Summary("Create new todo item")
  @Returns(200, { description: "OK (Created)" })
  async addTodo(@BodyParams() todo: TodoItem): Promise<TodoItem> {
    // FIXME: Add error handling!
    $log.info(`Creating new todo item: ${todo}`);
    if (isNil(todo)) {
      return null;
    } else {

    }
    const result = await MongooseService.resource.collection('todo').insert(todo);
    return result.ops[0];
  }

  /**
   * Update todo by id.
   *
   * @returns {Promise<string>}
   * @memberof TodoController
   */
  @Put("/:id")
  @Description("Update todo item by ID")
  @Summary("Update todo item by ID")
  @Returns(200, { description: "OK (Updated)" })
  @Returns(404, { description: "Todo item not found" })
  async updateTodo(@PathParams("id") id: string): Promise<string> {
    // TODO: Implementation.
    return null;
  }

  /**
   * Delete todo by id.
   *
   * @returns {Promise<string>}
   * @memberof TodoController
   */
  @Delete("/:id")
  @Description("Delete todo item by ID")
  @Summary("Delete todo item by ID")
  @Returns(200, { description: "OK (Deleted)" })
  @Returns(404, { description: "Todo item not found" })
  async deleteTodo(@PathParams("id") id: string): Promise<string> {
    // TODO: Implementation.
    return null;
  }
}
