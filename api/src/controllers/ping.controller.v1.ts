import { Controller, Get } from "@tsed/common";
import { Description, Returns, Summary } from "@tsed/swagger";
import { Deprecated } from "@tsed/core";

@Controller("/ping")
export class PingController {

  /**
   * Ping endpoint for testing conections.
   *
   * @returns {Promise<string>}
   * @memberof PingController
   */
  @Get("")
  @Description("Ping / Pong endpoint for testing.")
  @Summary("Ping endpoint should only be used for testing server health")
  @Returns(200, { description: "OK -> Ping / Pong" })
  async ping(): Promise<string> {
      return "pong!";
  }
}
