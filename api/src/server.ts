import { $log } from 'ts-log-debug';
import { Server } from 'app';

new Server()
    .start()
    .then(() => {})
    .catch((error: Error) => {
        $log.error(error.message);
    });
