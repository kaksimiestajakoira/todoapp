import { Connection, createConnection } from "mongoose";
import { Service } from "@tsed/common";
import config from "config/environment";

@Service()
export class MongooseService {
  /**
   * MongoDB Connection resource
   *
   * @static
   * @type {mongoose.Connection}
   * @memberof MongooseService
   */
  static resource: Connection;

  /**
   * Create MongoDB connection.
   *
   * @static
   * @returns {Promise<mongoose.Connection>}
   * @memberof MongooseService
   */
  static async connect(): Promise<Connection> {
    const mongoUrl = `mongodb://${config.database.host}:${config.database.port}`;

    if (MongooseService.resource) {
        return MongooseService.resource;
    }

    const db = await createConnection(mongoUrl, {
        user: config.database.username,
        pass: config.database.password
    });

    db.useDb("todo");

    MongooseService.resource = db;
    return db;
  }
}
