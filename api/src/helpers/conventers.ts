import { isNil } from "lodash";

export const stringToNumber = (value: string): number => (
  isNil(value) ? null : Number(value)
);
