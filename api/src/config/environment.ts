import { stringToNumber } from "helpers/conventers";

export interface Config {
  database: {
    host: string;
    port: number;
    username: string;
    password: string;
  }
}

const config: Config = {
  database: {
    host: process.env.DATABASE_HOST || "mongo",
    port: stringToNumber(process.env.DATABASE_PORT) || 27017,
    username: process.env.DATABASE_USER || "",
    password: process.env.DATABASE_PASSWORD || ""
  }
};

export default config;
