export interface TodoItem {
  id: string;
  user: string;
  done: boolean;
  text: string;
  color?: string;
}
